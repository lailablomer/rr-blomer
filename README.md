# Reproducible Research

This repo was created for the course 'Reproducible Research'. This course is offered to PhD students by the Université Grenoble Alpes, and on the online platform [FUN](https://www.fun-mooc.fr/). Reproducible research is about practicing and promoting best practices in research, both from a technical and an epistemological point of view. Whether you take notes, you produce figures, perform large computation or data analyses, Reproducible Research is of interest for every researcher.  
  
In this repository I share any assignments done in this course. 

## Module 3
In this data visualisation excersise, data on the purchasing power of English workers from the 16th to the 19th century is visualised. The graphs are based on the work of [William Playfair](https://en.wikipedia.org/wiki/William_Playfair), the inventor of the histogram.
