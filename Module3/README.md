# Purchasing power of English workers from the 16th to the 19th century 

This project visualises the purchasing power (defined as the quantity of wheat a worker can buy with a weekly salary) of English workers from the 16th to 19th century. The ojective is to re-produce charts made by [William Playfair](https://fr.wikipedia.org/wiki/William_Playfair)

## The data
The data is available in this repo, as Wheat.csv. The code (in R) to retrieve this data can be found [here](https://vincentarelbundock.github.io/Rdatasets/doc/HistData/Wheat.html). 

## The visualisation
All the visualisations are shared in Jupyter Notebook (analysis.ipynb). All code can be re-run and modified as needed.
